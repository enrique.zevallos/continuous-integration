# **Continuous Integration**
Is the process by which chunks of code from several developers is merged into the mainstream.

The purpose of this repository is documenting the necessary procedures that will allow us to test, build and further down the line, *deliver* software.
It is but the first piece in the CI/CD cycle that scopes **DevOps**.
The goal is to be able to go from idea to production, with just a **push**.

## CI best practices  :ok_hand:

These **Continuous Integration Best Practices** should be kept in mind, so as to avoid *merge hell*:
* Minimise branching
* Automate the build
* Enable Self-Testing for the build
* Commit at least daily
* Build every commit
* Build fast to react faster
* Mirror the production environment, in the test environment
* Make it available
* Auto Deploy

## Where to Start
### DevOps Overview
![WorkFlow](https://docs.gitlab.com/ee/ci/img/cicd_pipeline_infograph.png)
At zero stage, we have an app we've coded from zero. Whether it's php-based, js, ruby on rails, or any other language, that doesn't matter.
The first step towards Continuous Integration, is setting up a repository, with a version control software.
Of course, seeing that we are currently using **GitLab**, it's fair to assume we'll be using GitLab to host our repo, and git as a *vcs*.
This Documentation assumes you're familiar with the usual *git* operations and workflow, and thus we won't be covering how to work with it.
If you're not, you may want to [Try Git](https://try.github.io/levels/1/challenges/1)
#### GitLab
**GitLab** enables *CI* through an xml-based scripting language named *yml*, or *'yaml'* colloquially.
So if we want to run jobs to integrate our code, this is where to start.
```bash
git init repo
cd repo
touch .gitlab-ci.yml
vim .gitlab-ci.yml
# Configure how the application shall be built and tested in Gitlab
git add .
git commit -m "First Commit"
git add remote origin *remote repository url*
git remote -v
# Verifies what's just been done
git push origin master
```
Let's go through the commands we've executed.
We first initialize the repository that will later store our baseline code.
We enter said directory and create a file named *.gitlab-ci.yml* in the root of our repo, an *xml-based* script that will run the *pipeline*.
The main idea behind it, is telling the *GitLab Runner* which **jobs** need to be **executed** through a series of instructions.
By default, it'll have **three** stages, 
* Build
* Test
* Deploy

It's not compulsory to use all of these stages, stages with no jobs are simply ignored.
It certainly is a good practice for the team to use the Test stage as a *Testing Suite* because it gives developers immediate feedback.
This will allow us to rapidly realize whether we just broke the code or not.
There's a growing trend to use continuous delivery and continuous deployment to automatically deploy tested code to staging and production environments.
This shall be covered in the **Continuous Deployment** and **Continuous Delivery** sections of the documentation.

##### .gitlab-ci.yml